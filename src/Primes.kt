fun main(args: Array<String>) {
    for (i in 2..500) {
        print("Number $i ${if (checkPrime(i)) "is" else "is not"} a prime!\n")
    }
}

fun checkPrime(number: Int): Boolean {
    for (counter in 2 until number - 1) {
        if (number % counter == 0) return false
    }
    return true
}