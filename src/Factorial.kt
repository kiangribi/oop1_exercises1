import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.math.BigInteger

fun main(args: Array<String>) {
    print("Number to factor: ")
    var input = 0
    try {
        input = BufferedReader(InputStreamReader(System.`in`)).readLine().toInt()
    } catch (e: IOException) {
        println("Invalid input")
        System.exit(1)
    }
    if (input < 1) {
        println("Invalid input")
    }
    var value = BigInteger(Integer.toString(input))
    while (true) {
        val time = factorCountTime(value, true)
        System.out.printf("Time to factor '%d': %d ns (%d ms)\n", value, time, time / 1000000)
        value = value.add(BigInteger.ONE)
        Thread.sleep(500)
    }
}

fun factorCountTime(number: BigInteger, doubleFactor: Boolean): Long {
    val startMillis = System.nanoTime()
    val factor = factor(number, doubleFactor)
    System.out.printf("(Double) Factor of %d: %d\n", number, factor)
    return System.nanoTime() - startMillis
}

fun factor(number: BigInteger, doubleFactor: Boolean): BigInteger {
    var counter = number
    var value = BigInteger.ONE
    while (counter > BigInteger.ZERO) {
        value *= counter
        counter -= if (doubleFactor) BigInteger.TWO else BigInteger.ONE
    }
    return value
}