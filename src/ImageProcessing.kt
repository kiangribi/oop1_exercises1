import java.awt.Color

fun main(args: Array<String>){
    ImageViewer()
}

fun invert(pixels: Array<IntArray>): Array<Array<Int>> {
    val newImage = Array(pixels.size) {Array(pixels[0].size) {0} }
    for (y in pixels.indices) {
        for (x in pixels[0].indices) {
            newImage[y][x] = -pixels[y][x]
        }
    }
    return newImage
}

fun rotate(pixels: Array<IntArray>): Array<Array<Int>> {
    val newImage = Array(pixels[0].size) {Array(pixels.size) {0} }
    for (y in pixels.indices) {
        for (x in pixels[0].indices) {
            newImage[x][pixels.size - 1 - y] = pixels[y][x]
        }
    }
    return newImage
}

fun mirror(pixels: Array<IntArray>): Array<Array<Int>> {
    val newImage = Array(pixels.size) {Array(pixels[0].size) {0} }
    for (y in pixels.indices) {
        for (x in pixels[0].indices) {
            newImage[y][pixels[0].size - 1 - x] = pixels[y][x]
        }
    }
    return newImage
}

fun gray(pixels: Array<IntArray>): Array<Array<Int>> {
    val newImage = Array(pixels.size) {Array(pixels[0].size) {0} }
    for (y in pixels.indices) {
        for (x in 0 until pixels[0].size) {
            val color = Color(pixels[y][x])
            val avg = (color.red + color.green + color.blue) / 3
            newImage[y][x] = avg shl 16 or (avg shl 8) or avg
        }
    }
    return newImage
}